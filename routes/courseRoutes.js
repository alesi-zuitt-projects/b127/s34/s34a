const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseControllers')
const auth = require('../auth');

//Route for creating a course

router.post('/', auth.verify, (req,res) => {
	const data ={
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.addCourse(data).then(result => res.send(result));
})

//retrieve courses
router.get('/all', (req,res) => {

	courseController.getAllCourses().then(result => res.send(result))
})

//retrieve all true courses
router.get('/active', (req,res) => {
	courseController.getAllActive().then(result => res.send(result))
})

//retrieve spec course
router.get('/onecourse', (req,res) => {
	courseController.SpecificCourse(req.body).then(result=>res.send(result))
})

//retrive spec course by id
router.get('/:courseId', (req,res) =>{
	courseController.getOneCourse(req.params).then(result => res.send(result));
})

//update course
router.put('/:id', auth.verify, (req,res) => {
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.updateCourse(req.params.id, data).then(result => res.send(result))
})





module.exports = router;


