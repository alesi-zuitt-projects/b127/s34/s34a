const mongoose = require ('mongoose');

const courseSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Course is required"]
	},
	description: {
		type: String,
		required:[true, "description is required"]
	},
	price: {
		type: Number,
		required:[true, "price is required"]
	},

	//this is referencing due to the userId
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, 'userId is required']
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}
		}
	],
	isActive: {
		type: Boolean,
		default: true,
	},
	dateCreated:{
		type: Date,
		default: new Date()
	}

});

module.exports = mongoose.model("Course", courseSchema);